MonBras est une variante du projet MeArm

- C'est un bras articulé à assembler et à programmer
- Le mouvement est réalisé avec des servomoteurs de type MG90s
- La programmation peut etre réalisé à partir d'arduino
- Le fichier est au format svg
- Des marqueurs d'aide au montage on été ajoutée

Le projet de depard :
- MeArm V0.4 - Pocket Sized Robot Arm
- https://www.thingiverse.com/thing:360108